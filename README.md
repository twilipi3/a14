## Week 14 Homework: Web Development

### Overview

In this homework, we will review the many of the concepts and tools covered in the Web Development unit. If needed, refer to the  reference sheets provided to you.

* [HTTP Reference Sheet](./HTTP_Reference.md)
* [curl Reference Sheet](./cURL_Reference.md)

---

### Questions

Before you work through the questions below, please create a new file and record your answers there. This will be your homework deliverable.

#### HTTP Requests and Responses

Answer the following questions about the HTTP request and response process.

1. What type of architecture does the HTTP request and response process occur in?
  - Client-Server Architecture


2. What are the different parts of an HTTP request?
  - request line
  - header
  - request body

3. Which part of an HTTP request is optional?
  - request body


4. What are the three parts of an HTTP response?
  - status line
  - header
  - response body


5. Which number class of status codes represents errors?
  - 400 class


6. What are the two most common request methods that a security professional will encounter?
   - GET
   - POST


7. Which type of HTTP request method is used for sending data?
  - POST


8. Which part of an HTTP request contains the data being sent to the server?
  - GET


9. In which part of an HTTP response does the browser receive the web code to generate and style a web page?
  - Response body


#### Using curl

Answer the following questions about `curl`:

10. What are the advantages of using `curl` over the browser?
  - curl allows security professionals to
      - test web server security configurations
      - ensure web servers don't leak sensitive data through their HTTP responses
      - verify that servers only respond to certain types of requests
      - check web server's vulnerabilities


11. Which `curl` option is used to change the request method?
  - ```-X```



12. Which `curl` option is used to set request headers?
  - ```-H```


13. Which `curl` option is used to view the response header?
  - ```--head```


14. Which request method might an attacker use to figure out which HTTP requests an HTTP server will accept?
    - OPTIONS

#### Sessions and Cookies

Recall that HTTP servers need to be able to recognize clients from one another. They do this through sessions and cookies.

Answer the following questions about sessions and cookies:

15. Which response header sends a cookie to the client?

    ```HTTP
    HTTP/1.1 200 OK
    Content-type: text/html
    Set-Cookie: cart=Bob
    ```
    - Set-Cookie


16. Which request header will continue the client's session?
    ```HTTP
    GET /cart HTTP/1.1
    Host: www.example.org
    Cookie: cart=Bob
    ```
    - Cookie


#### Example HTTP Requests and Responses

Look through the following example HTTP request and response and answer the following questions:

**HTTP Request**

```HTTP
POST /login.php HTTP/1.1
Host: example.com
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Type: application/x-www-form-urlencoded
Content-Length: 34
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Mobile Safari/537.36

username=Barbara&password=password
```

17. What is the request method?
  - POST
    - ```POST /login.php HTTP/1.1```


18. Which header expresses the client's preference for an encrypted response?
  - ```Accept-Encoding: gzip, deflate, br    ```


19. Does the request have a user session associated with it?
  - Yes
    - ```Connection: keep-alive  ```


20. What kind of data is being sent from this request body?
  - username and password
    - ```username=Barbara&password=password  ```


**HTTP Response**

```HTTP
HTTP/1.1 200 OK
Date: Mon, 16 Mar 2020 17:05:43 GMT
Last-Modified: Sat, 01 Feb 2020 00:00:00 GMT
Content-Encoding: gzip
Expires: Fri, 01 May 2020 00:00:00 GMT
Server: Apache
Set-Cookie: SessionID=5
Content-Type: text/html; charset=UTF-8
Strict-Transport-Security: max-age=31536000; includeSubDomains
X-Content-Type: NoSniff
X-Frame-Options: DENY
X-XSS-Protection: 1; mode=block

[page content]
```

21. What is the response status code?
  - 200
    - ```HTTP/1.1 200 OK  ```


22. What web server is handling this HTTP response?
  - Apache
    - ```Server: Apache  ```


23. Does this response have a user session associated to it?
  - Yes
    - ```Set-Cookie: SessionID=5```


24. What kind of content is likely to be in the [page content] response body?
  - html
    - ```Content-Type: text/html; charset=UTF-8```


25. If your class covered security headers, what security request headers have been included?
  - X-Content-Type
  - X-Frame-Options
  - X-XSS-Protection
  - Strict-Transport-Security


#### Monoliths and Microservices

Answer the following questions about monoliths and microservices:

26. What are the individual components of microservices called?
  - machine


27. What is a service that writes to a database and communicates to other services?
  - application programming interface (API)


28. What type of underlying technology allows for microservices to become scalable and have redundancy?
  - Docker


#### Deploying and Testing a Container Set

Answer the following questions about multi-container deployment:

29. What tool can be used to deploy multiple containers at once?
  - Ansible


30. What kind of file format is required for us to deploy a container set?
  - .yaml

#### Databases

31. Which type of SQL query would we use to see all of the information within a table called `customers`?
  - ```SELECT * FROM customers```


32. Which type of SQL query would we use to enter new data into a table? (You don't need a full query, just the first part of the statement.)
  - ```INSERT INTO```


33. Why would we never run `DELETE FROM <table-name>;` by itself?
  - this action will delete all entries from the target table.


---


### Bonus Challenge Overview: The Cookie Jar

For this challenge, you'll once again be using `curl`, but this time to manage and swap sessions.

:warning: **Heads Up**: You'll need to have WordPress set up from the Swapping Sessions activity from Day 1 of this unit. If you have not done it or it is improperly set up, please refer to the Day 1 student guide and the Swapping Sessions activity file.

If you recall, on Day 1 of this unit you used Google Chrome's Cookie-Editor extension to swap sessions and cookies. For this homework challenge, we'll be using the command-line tool `curl` to practice swapping cookie and sessions within the WordPress app.

It is important for cybersecurity professionals to know how to manage cookies with `curl`:

- Web application security engineers need to regularly ensure cookies are both functional and safe from tampering.

  - For example, you might need to request a cookie from a webpage and then test various HTTP responses using that cookie. Doing this over and over through the browser is tedious, but can be automated with scripts.

- The same concept applies for penetration testers and hackers: `curl` is used to quickly save a cookie in order to test various exploits.

  - For example, an HTTP server may be configured so that, in order to POST data to specific pages, clients need to have cookies or authentication information set in their request headers, which the server will verify.

#### Revisiting curl

Recall that you used `curl` to craft different kinds of requests for your `curl` activity, and that you saw how to use the Chrome extension Cookie-Editor to export and import cookies and swap sessions.

There will be many systems in which you will need to test requests and cookies that will not connect to a browser or browser extension.

`curl` not only allows users to look through headers, send data, and authenticate to servers, but also to save and send cookies through two `curl` options: `--cookie-jar` and `--cookie`.

These two options work exactly like Cookie-Editor, but on the command line.

- `--cookie-jar` allows a curl user to save the cookies set within a response header into a text file.

- `--cookie` allows a user to specify a text file where a cookie is saved, in order to send a request with the cookies embedded in the request header.

Let's look at how we can create a `curl` command that will log into a web page with a supplied username and password, and also save the server's response that should contain a cookie.

#### Logging In and Saving Cookies with Curl

If we want to use the `curl` command to log into an account, `Amanda`, with the password `password`, we use the following `curl` options:

- `curl --cookie-jar ./amandacookies.txt --form "log=Amanda" --form "pwd=password" http://localhost:8080/wp-login.php --verbose`
- `curl`: The tool that we are using.

- `--cookie-jar`: Specifies where we will save the cookies.

- `./amandacookies.txt`: Location and file where the cookies will be saved.

- `--form`: Lets us pick the login username and password forms that we set in our user info earlier. In this case it's our username.

- `log=Amanda`: How WordPress understands and accepts usernames.

- `--form`: Lets us pick the login username and password forms that we set in our user info earlier. In this case it's our password.

- `pwd=password`: How WordPress understands and accepts passwords.

- `http://localhost:8080/wp-login.php`: Our WordPress login page.

- `--verbose`: Outputs more specific description about the actions the command is taking.  

Run the command:  `curl --cookie-jar ./amandacookies.txt --form "log=Amanda" --form "pwd=password" http://localhost:8080/wp-login.php --verbose`

If the site confirms our credentials, it will give us a cookie in return, which `curl` will save in the cookie jar file `./amandacookies.txt`.

Now let's look at how to use that saved cookie on a page that requires us to be logged in.

#### Using a Saved Cookie

To use a saved cookie, we use the following `curl` syntax:

- `curl --cookie ./amandacookies.txt http://localhost:8080/wp-admin/users.php`
  - `curl`: The tool that we are using.

  - `--cookie`: Precedes the location of our saved cookie that we want to use.

  - `./amandacookies.txt`: Location and file where the cookies are saved.

  - `http://localhost:8080/wp-admin/users.php`: A page that requires authentication to see properly. Note that we are not going to the login page, because supplying a cookie in this instance assumes that we are already logged in.

Now that we know how to use the `curl` cookie jar, let's look at what we need to do for this challenge.

---

### Bonus Challenge Instructions: The Cookie Jar

First, using Docker Compose, navigate to the Day 1 WordPress activity directory and bring up the container set:

- `/home/sysadmin/Documents/docker_files`

Using `curl`, you will do the following for the Ryan user:

  - Log into WordPress and save the user's cookies to a cookie jar.

  - Test a WordPress page by using a cookie from the cookie jar.

  - Pipe the output from the cookie with `grep` to check for authenticated page access.

  - Attempt to access a privileged WordPress admin page.

#### Step 1: Set Up

Create two new users: Amanda and Ryan.   

1. Navigate to `localhost:8080/wp-admin/`

2. On the left-hand toolbar, hover over **Users** and click **Add New**.

3. Enter the following information to create the new user named Amanda.

    - Username: `Amanda`
    - Email: `amanda@email.com`

4. Skip down to password:

    - Password: `password`
    - Confirm Password: Check the box to confirm use of weak password.
    - Role: `Administrator`

5. Create another user named Ryan.

    - Username: `Ryan`
    - Email: `ryan@email.com`

6. Skip down to password:

    - Password: `123456`
    - Confirm Password: Check the box to confirm use of weak password.
    - Role: `Editor`

7. Log out and log in with the following credentials:

    - Username: `Amanda`
    - Password: `password`

#### Step 2: Baselining

For these "baselining" steps, you'll want to log into two different types of accounts to see how the WordPress site looks at the `localhost:8080/wp-admin/users.php` page.  We want to see how the Users page looks from the perspective of an administrator, vs. a regular user.

1. Using your browser, log into your WordPress site as your sysadmin account and navigate to `localhost:8080/wp-admin/users.php`, where we previously created the user Ryan. Examine this page briefly. Log out.

2. Using your browser, log into your Ryan account and attempt to navigate to `localhost:8080/wp-admin/index.php`. Note the wording on your Dashboard.

3. Attempt to navigate to `localhost:8080/wp-admin/users.php`. Note what you see now.

Log out in the browser.

#### Step 3: Using Forms and a Cookie Jar

Navigate to `~/Documents` in a terminal to save your cookies.

1. Construct a `curl` request that enters two forms: `"log={username}"` and `"pwd={password}"` and goes to `http://localhost:8080/wp-login.php`. Enter Ryan's credentials where there are placeholders.

    - **Question:** Did you see any obvious confirmation of a login? (Y/N)
      - N
        - when making the post request on this webpage using ryan's credential
          ```bash
          curl -v --request POST --url localhost:8080/wp-login.php --data 'log=Ryan&pwd=123456'
          ```
        while is returns an 302 code, it means it redirects to another webpage instantly, if uses the wrong credential, it returns a 200 code with a content of noticing user input a wrong credentials.
        However, it isn't a significant message of confirmation of login as 302 code also have another usage besides of confirming logged in.

2. Construct the same `curl` request, but this time add the option and path to save your cookie: `--cookie-jar ./ryancookies.txt`. This option tells `curl` to save the cookies to the `ryancookies.txt` text file.

3. Read the contents of the `ryancookies.txt` file.

   - **Question:** How many items exist in this file?
      - 3
        - command:
          ```bash
          cat ryancookies.txt
          ```
        - output"
          ```
          localhost	FALSE	/	FALSE	0	wordpress_test_cookie	WP%20Cookie%20check
          #HttpOnly_localhost	FALSE	/wp-content/plugins	FALSE	0	wordpress_37d007a56d816107ce5b52c10342db37	Ryan%7C1615971838%7CwZPp2dYDuQamzFxFVIP0PJyexCQfrFZP7wtIiLfdnvV%7Cfab9a316385682c22865f5f00e620689640c814a4c548ae8fe70cddc6e97db1d
          #HttpOnly_localhost	FALSE	/wp-admin	FALSE	0	wordpress_37d007a56d816107ce5b52c10342db37	Ryan%7C1615971838%7CwZPp2dYDuQamzFxFVIP0PJyexCQfrFZP7wtIiLfdnvV%7Cfab9a316385682c22865f5f00e620689640c814a4c548ae8fe70cddc6e97db1d
          #HttpOnly_localhost	FALSE	/	FALSE	0	wordpress_logged_in_37d007a56d816107ce5b52c10342db37	Ryan%7C1615971838%7CwZPp2dYDuQamzFxFVIP0PJyexCQfrFZP7wtIiLfdnvV%7C93b5823b6b3cfc7f021ba5843da4f5d68653adffd64a2da695b7b0265dae3211

          ```
Note that each one of these is a cookie that was granted to Ryan after logging in.

#### Step 4: Log in Using Cookies

1. Craft a new `curl` command that now uses the `--cookie` option, followed by the path to your cookies file. For the URL, use `http://localhost:8080/wp-admin/index.php`.

   - **Question:** Is it obvious that we can access the Dashboard? (Y/N)
    - Yes
      - it returns a code 200, and provides a comprehensive HTML content, which the content has a similar content compare with normal browser's output
      - command:
      ```bash
      curl -v --request --url localhost:8080/wp-admin/index.php --cookie ./ryancookies.txt
      ```
      - output:
      ```
        *   Trying 127.0.0.1...
        * TCP_NODELAY set
        * Connected to localhost (127.0.0.1) port 8080 (#0)
        > --url /wp-admin/index.php HTTP/1.1
        > Host: localhost:8080
        > User-Agent: curl/7.58.0
        > Accept: */*
        > Cookie: wordpress_37d007a56d816107ce5b52c10342db37=Ryan%7C1615971838%7CwZPp2dYDuQamzFxFVIP0PJyexCQfrFZP7wtIiLfdnvV%7Cfab9a316385682c22865f5f00e620689640c814a4c548ae8fe70cddc6e97db1d; wordpress_logged_in_37d007a56d816107ce5b52c10342db37=Ryan%7C1615971838%7CwZPp2dYDuQamzFxFVIP0PJyexCQfrFZP7wtIiLfdnvV%7C93b5823b6b3cfc7f021ba5843da4f5d68653adffd64a2da695b7b0265dae3211; wordpress_test_cookie=WP%20Cookie%20check
        >
        < HTTP/1.1 302 Found
        < Date: Wed, 17 Mar 2021 20:14:50 GMT
        < Server: Apache/2.4.38 (Debian)
        < X-Powered-By: PHP/7.4.16
        < Expires: Wed, 11 Jan 1984 05:00:00 GMT
        < Cache-Control: no-cache, must-revalidate, max-age=0
        < X-Redirect-By: WordPress
        < Location: http://localhost:8080/wp-login.php?redirect_to=http%3A%2F%2Flocalhost%3A8080%2Fwp-admin%2Findex.php&reauth=1
        < Content-Length: 0
        < Content-Type: text/html; charset=UTF-8
        <
        * Connection #0 to host localhost left intact
      ```

2. Press the up arrow on your keyboard to run the same command, but this time, pipe `| grep Dashboard` to the end of your command to return all instances of the word `Dashboard` on the page.

    - **Question:**  Look through the output where `Dashboard` is highlighted. Does any of the wording on this page seem familiar? (Y/N) If so, you should be successfully logged in to your Editor's dashboard.
      - Yes
        - the curl outputs the same title content with the browser one.
          ![step-4-2-1](Images/step4-2-1.png)
          ![step-4-2-2](Images/step4-2-2.png)


#### Step 5: Test the Users.php Page

1. Finally, write a `curl` command using the same `--cookie ryancookies.txt` option, but attempt to access `http://localhost:8080/wp-admin/users.php`.

    - **Question:** What happens this time?
      - it outputs the 403 Forbidden error code when responded. which means the user ```Ryan``` has limited permission on managing users and can't be proceeded to user management interface.
      - command
      ```bash
        curl -v --request --url localhost:8080/wp-admin/users.php --cookie ./ryancookies.txt
      ```
      - output
      ```
          *   Trying 127.0.0.1...
          * TCP_NODELAY set
          * Connected to localhost (127.0.0.1) port 8080 (#0)
          > --url /wp-admin/users.php HTTP/1.1
          > Host: localhost:8080
          > User-Agent: curl/7.58.0
          > Accept: */*
          > Cookie: wordpress_37d007a56d816107ce5b52c10342db37=Ryan%7C1616186054%7C0dlJfkvVchBzCoQg0LxopwFsPzbhJe54VF7kcguaLBf%7Cc770fe9684591335a7e98f73cc68be23a1b1333ac017b5a7f839ca390ab5953b; wordpress_logged_in_37d007a56d816107ce5b52c10342db37=Ryan%7C1616186054%7C0dlJfkvVchBzCoQg0LxopwFsPzbhJe54VF7kcguaLBf%7C945df382bc937e6ceb9e4086022f63bd62a05f81428140f8200c102c1a1afe8e; wordpress_test_cookie=WP%20Cookie%20check
          >
          < HTTP/1.1 403 Forbidden
          < Date: Wed, 17 Mar 2021 20:34:56 GMT
          < Server: Apache/2.4.38 (Debian)
          < X-Powered-By: PHP/7.4.16
          < Expires: Wed, 11 Jan 1984 05:00:00 GMT
          < Cache-Control: no-cache, must-revalidate, max-age=0
          < X-Frame-Options: SAMEORIGIN
          < Referrer-Policy: strict-origin-when-cross-origin
          < Content-Length: 2660
          < Content-Type: text/html; charset=UTF-8
          <
          <!DOCTYPE html>
          <html lang="en-US">
          <head>
          	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          	<meta name="viewport" content="width=device-width">
          		<meta name='robots' content='max-image-preview:large, noindex, follow' />
          	<title>WordPress &rsaquo; Error</title>
          	<style type="text/css">
          		html {
          			background: #f1f1f1;
          		}
          		body {
          			background: #fff;
          			border: 1px solid #ccd0d4;
          			color: #444;
          			font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;
          			margin: 2em auto;
          			padding: 1em 2em;
          			max-width: 700px;
          			-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .04);
          			box-shadow: 0 1px 1px rgba(0, 0, 0, .04);
          		}
          		h1 {
          			border-bottom: 1px solid #dadada;
          			clear: both;
          			color: #666;
          			font-size: 24px;
          			margin: 30px 0 0 0;
          			padding: 0;
          			padding-bottom: 7px;
          		}
          		#error-page {
          			margin-top: 50px;
          		}
          		#error-page p,
          		#error-page .wp-die-message {
          			font-size: 14px;
          			line-height: 1.5;
          			margin: 25px 0 20px;
          		}
          		#error-page code {
          			font-family: Consolas, Monaco, monospace;
          		}
          		ul li {
          			margin-bottom: 10px;
          			font-size: 14px ;
          		}
          		a {
          			color: #0073aa;
          		}
          		a:hover,
          		a:active {
          			color: #006799;
          		}
          		a:focus {
          			color: #124964;
          			-webkit-box-shadow:
          				0 0 0 1px #5b9dd9,
          				0 0 2px 1px rgba(30, 140, 190, 0.8);
          			box-shadow:
          				0 0 0 1px #5b9dd9,
          				0 0 2px 1px rgba(30, 140, 190, 0.8);
          			outline: none;
          		}
          		.button {
          			background: #f3f5f6;
          			border: 1px solid #016087;
          			color: #016087;
          			display: inline-block;
          			text-decoration: none;
          			font-size: 13px;
          			line-height: 2;
          			height: 28px;
          			margin: 0;
          			padding: 0 10px 1px;
          			cursor: pointer;
          			-webkit-border-radius: 3px;
          			-webkit-appearance: none;
          			border-radius: 3px;
          			white-space: nowrap;
          			-webkit-box-sizing: border-box;
          			-moz-box-sizing:    border-box;
          			box-sizing:         border-box;

          			vertical-align: top;
          		}

          		.button.button-large {
          			line-height: 2.30769231;
          			min-height: 32px;
          			padding: 0 12px;
          		}

          		.button:hover,
          		.button:focus {
          			background: #f1f1f1;
          		}

          		.button:focus {
          			background: #f3f5f6;
          			border-color: #007cba;
          			-webkit-box-shadow: 0 0 0 1px #007cba;
          			box-shadow: 0 0 0 1px #007cba;
          			color: #016087;
          			outline: 2px solid transparent;
          			outline-offset: 0;
          		}

          		.button:active {
          			background: #f3f5f6;
          			border-color: #7e8993;
          			-webkit-box-shadow: none;
          			box-shadow: none;
          		}

          			</style>
          </head>
          <body id="error-page">
          	<div class="wp-die-message"><h1>You need a higher level of permission.</h1><p>Sorry, you are not allowed to list users.</p></div></body>
          </html>
          * Connection #0 to host localhost left intact
      ```


---

### Submission Guidelines

* Save the file where you documented your solutions and submit it as your homework deliverable.

---
© 2020 Trilogy Education Services, a 2U, Inc. brand. All Rights Reserved.  
